const { request } = require('express');
const Task = require('../models/Task.js');

// GET
module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    });
};

// CREATE / POST
module.exports.createTask = (requestBody) => {

    let newTask = new Task({
        name: requestBody.name //name from postman input
    });
    
    return newTask.save().then((savedTasks, error) => {
        if (error){
            return error
        }
            return 'Task created successfully'

    });
};

// UPDATE
module.exports.updateTask = (taskId, newValue) => {

    return Task.findById(taskId).then((result, error) => {
        if (error){
            return error
        }
        result.name = newValue.name

        return result.save().then((updatedTask, error) => {
            if (error){
                return error
            } else {
                return updatedTask
            }
        });
    });
};

// DELETE
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
        if (error){
            return error
        } else {
            return deletedTask
        }
    });
};

// Retrieving One Task
module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((retrievedOneTask, error) => {
        if(error){
            return error
        } else {
            return retrievedOneTask
        }
    });
};