const { request, response } = require('express');
const express = require('express');
const TaskController = require('../controllers/TaskController.js');
const Task = require('../models/Task.js');
const router = express.Router();

// GET
router.get('/', (request, response) => {
    // business logic here
    TaskController.getAllTasks().then((tasks) => response.send(tasks));
});

// POST / CREATE
router.post('/create', (request, response) => {
    TaskController.createTask(request.body).then((task) => response.send(task))
});

// UPDATE
router.put('/:id/update', (request, response) => {
    TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask));
});

// DELETE
router.delete('/:id/delete', (request, response) => {
    TaskController.deleteTask(request.params.id).then((deletedTask) => response.send(deletedTask));
});

// Retrieve One Task by Id
router.get('/:id', (request, response) => {
    TaskController.getTask(request.params.id).then((oneTask) => response.send(oneTask));
});

module.exports = router

