// Import dependencies
    const express = require('express');
    const mongoose = require('mongoose');
    const dotenv = require('dotenv');
   
    const taskRoutes = require('./routes/taskRoutes.js')

// Initialize dotenv for usage
    dotenv.config();

// Express Setup
    const app = express();
    const port = 3001;
    app.use(express.json());
    app.use(express.urlencoded({extended: true}));

// Mongoose Setup
    // mongoose.connect(`mongodb://127.0.0.1:27017/s36-discussion`
    mongoose.connect(`mongodb+srv://michella:${process.env.MONGODB_PASSWORD}@cluster0.6wklp.mongodb.net/s36-discussion?retryWrites=true&w=majority`,
        {
            useNewUrlParser : true,
            useUnifiedTopology : true
        }
    );

    let db = mongoose.connection
    db.on('error', () => console.error('Connection Error :('));
    db.on('open', () => console.log('Connection to MongoDB!'));

// Calling Routes
    app.use('/api/tasks', taskRoutes);

// Listen to port
    app.listen(port, () => console.log(`Server in now running at port ${port}`));